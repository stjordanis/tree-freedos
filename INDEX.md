# Tree

Graphically displays the folder structure of a drive or path. Support for message catalogs (different languages) using cats, and can be compiled for both Windows NT/9x and DOS.


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## TREE.LSM

<table>
<tr><td>title</td><td>Tree</td></tr>
<tr><td>version</td><td>3.7.2a</td></tr>
<tr><td>entered&nbsp;date</td><td>2002-09-03</td></tr>
<tr><td>summary</td><td>Graphically displays the folder structure of a drive or path. Support for message catalogs (different languages) using cats, and can be compiled for both Windows NT/9x and DOS.</td></tr>
<tr><td>description</td><td>Graphically displays the folder structure of a drive or path.</td></tr>
<tr><td>keywords</td><td>tree, FreeDOS, DOS, Win32</td></tr>
<tr><td>author</td><td>Dave Dunfield &lt;dave@dunfield.com&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Kenneth J. Davis &lt;jeremyd@computer.org&gt;</td></tr>
<tr><td>primary&nbsp;site</td><td>https://github.com/FDOS/tree</td></tr>
<tr><td>defunt&nbsp;site</td><td>http://www.darklogic.org/fdos/projects/tree/</td></tr>
<tr><td>platforms</td><td>DOS (TC2, TC++1, TC30, BC31, BC45, MicroC v3*, Pacific C*, Digital Mars), Win32 (VC5*, BC45, BCC55, Digital Mars) * MicroC and Pacific C do not support Cats, VC5 requires minor Cats change.</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2 (using public domain findfile by Jeremy Davis and optional use of LGPL catgets by Jim Hall)](LICENSE)</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Tree</td></tr>
</table>
